# This is the server side of my second year course work

## Setup

### Copy example of configuration file.

Windows:
```
copy config_example.py config.py
```

Linux/MacOS:
```
cp config_example.py config.py
```

### Now you have config.py file in the root folder. Tune it.

### Create Python virtual environment

```
python3 -m venv env
```

### Activate it

Windows:
```
./env/Scripts/activate
```

Linux/MacOS:
```
. ./env/bin/activate
```

### Install requirements

```
pip3 install -r requirements.txt
```

### Create PostgreSQL database

### Create tables

```
flask db init

flask db migrate

flask db upgrade
```

### Run the project
```
python main.py
```
