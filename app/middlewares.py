from functools import wraps

from flask import request
from flask_login import current_user

from app import app
from config import CLIENT_URL


@app.after_request
def add_cors_headers(response):
    response.headers['Access-Control-Allow-Origin'] = CLIENT_URL
    response.headers['Access-Control-Allow-Credentials'] = 'true'
    response.headers['Access-Control-Allow-Headers'] = 'Content-Type'

    return response


def required_fields(fields, data_source='json'):
    def decorator(func):
        @wraps(func)
        def _decorator(*args, **kwargs):
            if data_source == 'json':
                data = request.get_json(force=True)
            elif data_source == 'form':
                data = request.form

            for field in fields:
                if field not in data:
                    print(f'{request.url} => required field "{field}" is empty')
                    return {
                        'status': 'fail',
                        'message': f'{field} is empty'
                    }

            return func(*args, **kwargs)

        return _decorator

    return decorator


def login_required(admin=False):
    def decorator(func):
        @wraps(func)
        def _decorator(*args, **kwargs):
            if current_user.is_anonymous:
                return {
                    'status': 'fail',
                    'message': 'Please log in or sign up'
                }

            if admin and not current_user.is_admin:
                return {
                    'status': 'fail',
                    'message': 'You are not an administrator'
                }

            return func(*args, **kwargs)

        return _decorator

    return decorator
