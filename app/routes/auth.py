from flask import request
from flask_login import current_user, login_user, logout_user

from app import app, bcrypt, db
from app.models import User, Basket
from app.middlewares import required_fields, login_required


@app.route('/auth/sign_up', methods=['POST'])
@required_fields(['name', 'email', 'phone', 'password', 'remember'])
def sign_up():
    if not current_user.is_anonymous:
        return {
            'status': 'fail',
            'message': 'User is already logged in'
        }

    data = request.get_json(force=True)

    name = data['name']
    surname = data['surname']
    email = data['email'].lower()
    phone = data['phone']
    password = data['password']
    remember = data['remember']

    if User.query.filter_by(email=email).count() > 0:
        return {
            'status': 'fail',
            'message': 'User with this email is already registered'
        }
    if User.query.filter_by(phone=phone).count() > 0:
        return {
            'status': 'fail',
            'message': 'User with this phone is already registered'
        }
    if len(password) < 8:
        return {
            'status': 'fail',
            'message': 'Password should contain at least 8 symbols'
        }

    user = User(
        name=name,
        surname=surname,
        email=email,
        phone=phone,
        password_hash=bcrypt.generate_password_hash(password).decode()
    )
    basket = Basket(user.id)
    db.session.add(basket)
    db.session.add(user)
    db.session.commit()

    login_user(user, remember=remember)

    return {
        'status': 'success'
    }


@app.route('/auth/login', methods=['POST'])
@required_fields(['login', 'password', 'remember'])
def login_func():
    if not current_user.is_anonymous:
        return {
            'status': 'fail',
            'message': 'User is already logged in'
        }

    data = request.get_json(force=True)

    login = data['login'].lower()
    password = data['password']
    remember = data['remember']

    if '@' in login:
        user_query = User.query.filter_by(email=login)
    else:
        user_query = User.query.filter_by(phone=login)

    if user_query.count() == 0:
        return {
            'status': 'failed',
            'message': 'Wrong email or phone'
        }

    user = user_query.first()
    if not bcrypt.check_password_hash(user.password_hash, password):
        return {
            'status': 'failed',
            'message': 'Wrong password'
        }

    login_user(user, remember=remember)

    return {
        'status': 'success'
    }


@app.route('/auth/logout', methods=['POST'])
def logout():
    logout_user()

    return {
        'status': 'success'
    }


@app.route('/auth/get_user_data', methods=['POST'])
def get_user_data():
    if current_user.is_anonymous:
        return {
            'is_authenticated': False
        }

    return {
        'is_authenticated': True,
        **current_user.json
    }


@app.route('/auth/update_user_data', methods=['POST'])
@login_required()
@required_fields(['name', 'email', 'phone'])
def update_user_data():
    data = request.get_json(force=True)

    name = data['name']
    surname = data['surname']
    email = data['email'].lower()
    phone = data['phone']

    if User.query.filter_by(email=email).count() > 0 and User.query.filter_by(email=email).first() != current_user:
        return {
            'status': 'fail',
            'message': 'User with this email is already registered'
        }
    if User.query.filter_by(phone=phone).count() > 0 and User.query.filter_by(phone=phone).first() != current_user:
        return {
            'status': 'fail',
            'message': 'User with this phone is already registered'
        }

    current_user.name = name
    current_user.surname = surname
    current_user.email = email
    current_user.phone = phone
    db.session.commit()

    return {
        'status': 'success'
    }


@app.route('/auth/change_password', methods=['POST'])
@login_required()
@required_fields(['password', 'newPassword'])
def change_password():
    data = request.get_json(force=True)

    password = data['password']
    new_password = data['newPassword']

    if not bcrypt.check_password_hash(current_user.password_hash, password):
        return {
            'status': 'fail',
            'message': 'Incorrect current password'
        }

    current_user.password_hash = bcrypt.generate_password_hash(new_password).decode()
    db.session.commit()

    return {
        'status': 'success'
    }
