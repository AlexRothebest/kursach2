from flask import request
from flask_login import current_user

from app import app, db
from app.models import Product, Category, Manufacturer, Purchase
from app.middlewares import login_required, required_fields


@app.route('/products/all', methods=['POST'])
@login_required()
def all_products():
    return {
        'products': [product.json for product in Product.query.filter_by(basket_id=None, purchase_id=None)],
        'categories': [category.json for category in Category.query.all()],
        'manufacturers': [manufacturer.json for manufacturer in Manufacturer.query.all()],
        'purchases': [purchase.json for purchase in Purchase.query.filter_by(user_id=current_user.id    )]
    }


@app.route('/products/get_filtered', methods=['POST'])
@login_required()
@required_fields(['product_name', 'category_name', 'manufacturer_name', 'price_from', 'price_to', 'order_by'])
def products_get_filtered():
    data = request.get_json(force=True)

    product_name = data['product_name']
    category_name = data['category_name']
    manufacturer_name = data['manufacturer_name']
    price_from = data['price_from']
    price_to = data['price_to']
    order_by = data['order_by']

    products = Product.query.filter_by(basket_id=None, purchase_id=None)

    if product_name:
        products = products.filter(Product.name.contains(product_name))
    if category_name:
        products = products.join(Category).filter(Category.name.contains(category_name))
    if manufacturer_name:
        products = products.join(Manufacturer).filter(Manufacturer.name.contains(manufacturer_name))
    if price_from:
        products = products.filter(Product.price >= price_from)
    if price_to:
        products = products.filter(Product.price <= price_to)

    if order_by == 'productPrice':
        products = products.order_by(Product.price)
    if order_by == 'productName':
        products = products.order_by(Product.name)
    if order_by == 'categoryName':
        products = products.join(Category).order_by(Category.name)
    if order_by == 'manufacturerName':
        products = products.join(Manufacturer).order_by(Manufacturer.name)

    return {
        'products': [product.json for product in products]
    }


@app.route('/purchases/all', methods=['POST'])
@login_required()
def all_purchases():
    return {
        'purchases': [purchase.json for purchase in Purchase.query.filter_by(user=current_user)]
    }


@app.route('/products/add_to_basket', methods=['POST'])
@login_required()
@required_fields(['product_id'])
def add_product_to_basket():
    data = request.get_json(force=True)

    product_id = data['product_id']
    product = Product.query.get(product_id)
    current_user.basket.products.append(product)
    db.session.commit()

    return {
        'status': 'success'
    }


@app.route('/products/remove_from_basket', methods=['POST'])
@login_required()
@required_fields(['product_id'])
def remove_product_from_basket():
    data = request.get_json(force=True)

    product_id = data['product_id']
    product = Product.query.get(product_id)
    current_user.basket.products.remove(product)
    db.session.commit()

    return {
        'status': 'success'
    }


@app.route('/products/confirm_purchase', methods=['POST'])
@login_required()
def confirm_purchase():
    if len(current_user.basket.products) == 0:
        return {
            'status': 'fail',
            'message': 'Basket is empty'
        }

    purchase = Purchase(
        user=current_user,
        products=current_user.basket.products
    )
    db.session.add(purchase)
    current_user.basket.products = []
    db.session.commit()

    return {
        'status': 'success'
    }


@app.route('/update_category', methods=['POST'])
@login_required(admin=True)
@required_fields(['category_id', 'new_category_name'])
def update_category():
    data = request.get_json(force=True)

    category_id = data['category_id']
    new_category_name = data['new_category_name']
    category = Category.query.get(category_id)
    category.name = new_category_name
    db.session.commit()

    return {
        'status': 'success'
    }


@app.route('/delete_category', methods=['POST'])
@login_required(admin=True)
@required_fields(['category_id'])
def delete_category():
    data = request.get_json(force=True)

    category_id = data['category_id']
    Product.query.filter_by(category_id=category_id).delete()
    Category.query.filter_by(id=category_id).delete()
    db.session.commit()

    return {
        'status': 'success'
    }


@app.route('/update_manufacturer', methods=['POST'])
@login_required(admin=True)
@required_fields(['manufacturer_id', 'new_manufacturer_name'])
def update_manufacturer():
    data = request.get_json(force=True)

    manufacturer_id = data['manufacturer_id']
    new_manufacturer_name = data['new_manufacturer_name']
    manufacturer = Manufacturer.query.get(manufacturer_id)
    manufacturer.name = new_manufacturer_name
    db.session.commit()

    return {
        'status': 'success'
    }


@app.route('/delete_manufacturer', methods=['POST'])
@login_required(admin=True)
@required_fields(['manufacturer_id'])
def delete_manufacturer():
    data = request.get_json(force=True)

    manufacturer_id = data['manufacturer_id']
    Product.query.filter_by(manufacturer_id = manufacturer_id).delete()
    Manufacturer.query.filter_by(id=manufacturer_id).delete()
    db.session.commit()

    return {
        'status': 'success'
    }
