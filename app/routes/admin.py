from datetime import datetime, timedelta

from docx import Document
from docx.enum.text import WD_ALIGN_PARAGRAPH
from docx.enum.table import WD_ALIGN_VERTICAL
from flask import request, render_template_string
from flask_mail import Message

from app import app, db, mail
from app.models import Product, Manufacturer, Category, User
from app.middlewares import required_fields, login_required
from config import APIKEY, MAIL_USERNAME, APP_URL


@app.route('/admin/products/add', methods=['POST'])
@login_required(admin=True)
@required_fields(['name', 'price', 'isNewCategory', 'isNewManufacturer'], data_source='form')
def add_product():
    data = request.form

    name = data['name']
    price = round(float(data['price']), 2)
    is_new_category = data['isNewCategory'] == 'true'
    is_new_manufacturer = data['isNewManufacturer'] == 'true'

    if price <= 0:
        return {
            'status': 'fail',
            'message': 'Price less than or equal to 0'
        }

    if is_new_category:
        if 'newCategoryName' not in data or not data['newCategoryName']:
            return {
                'status': 'fail',
                'message': 'Category name is empty'
            }

        new_category_name = data['newCategoryName']
        new_category = Category(name=new_category_name)
        category_id = new_category.id
        db.session.add(new_category)
        db.session.commit()
    else:
        if 'categoryId' not in data:
            return {
                'status': 'fail',
                'message': 'Category ID is empty'
            }

        category_id = data['categoryId']

    if is_new_manufacturer:
        if 'newManufacturerName' not in data:
            return {
                'status': 'fail',
                'message': 'Manufacturer name is empty'
            }

        new_manufacturer_name = data['newManufacturerName']
        new_manufacturer = Manufacturer(name=new_manufacturer_name)
        manufacturer_id = new_manufacturer.id
        db.session.add(new_manufacturer)
        db.session.commit()
    else:
        if 'manufacturerId' not in data:
            return {
                'status': 'fail',
                'message': 'Manufacturer ID is empty'
            }

        manufacturer_id = data['manufacturerId']

    new_product = Product(
        name=name,
        price=price,
        category_id=category_id,
        manufacturer_id=manufacturer_id
    )

    if 'photo' in request.files:
        photo = request.files['photo']

        new_product_id = new_product.id.replace('urn:uuid:', '')
        photo_extension = photo.filename.split('.')[-1]
        filename = f'{new_product_id}.{photo_extension}'

        photo.save(f'./app/static/img/{filename}')

        new_product.photo_path = f'/static/img/{filename}'
    else:
        new_product.photo_path = '/static/img/default_product_photo.png'

    db.session.add(new_product)
    db.session.commit()

    return {
        'status': 'success'
    }


@app.route('/admin/products/edit', methods=['POST'])
@login_required(admin=True)
@required_fields(['product_id', 'name', 'price', 'is_new_category', 'is_new_manufacturer'], data_source='form')
def edit_product():
    data = request.form

    product_id = data['product_id']
    name = data['name']
    price = round(float(data['price']), 2)
    is_new_category = data['is_new_category'] == 'true'
    is_new_manufacturer = data['is_new_manufacturer'] == 'true'

    if price <= 0:
        return {
            'status': 'fail',
            'message': 'Price less than or equal to 0'
        }

    if is_new_category:
        if 'new_category_name' not in data or not data['new_category_name']:
            return {
                'status': 'fail',
                'message': 'Category name is empty'
            }

        new_category_name = data['new_category_name']
        new_category = Category(name=new_category_name)
        category_id = new_category.id
        db.session.add(new_category)
        db.session.commit()
    else:
        if 'category_id' not in data:
            return {
                'status': 'fail',
                'message': 'Category ID is empty'
            }

        category_id = data['category_id']

    if is_new_manufacturer:
        if 'new_manufacturer_name' not in data:
            return {
                'status': 'fail',
                'message': 'Manufacturer name is empty'
            }

        new_manufacturer_name = data['new_manufacturer_name']
        new_manufacturer = Manufacturer(name=new_manufacturer_name)
        manufacturer_id = new_manufacturer.id
        db.session.add(new_manufacturer)
        db.session.commit()
    else:
        if 'manufacturer_id' not in data:
            return {
                'status': 'fail',
                'message': 'Manufacturer ID is empty'
            }

        manufacturer_id = data['manufacturer_id']

    print(product_id)

    product = Product.query.get(product_id)

    product.name = name
    product.price = price
    product.category_id = category_id
    product.manufacturer_id = manufacturer_id

    if 'photo' in request.files:
        photo = request.files['photo']

        photo_extension = photo.filename.split('.')[-1]
        filename = f'{product_id}.{photo_extension}'

        photo.save(f'./app/static/{filename}')

        product.photo_path = f'/static/{filename}'

    db.session.commit()

    return {
        'status': 'success'
    }


@app.route('/admin/products/delete', methods=['POST'])
@login_required(admin=True)
@required_fields(['product_id'])
def delete_product():
    data = request.get_json(force=True)

    product_id = data['product_id']

    products = Product.query.filter_by(id=product_id)
    if products.count() == 0:
        return {
            'status': 'fail',
            'message': 'Product with this ID does not exist'
        }

    products.delete()
    db.session.commit()

    return {
        'status': 'success'
    }


@app.route('/admin/get_products_by_categories_report', methods=['POST'])
@login_required(admin=True)
def get_products_by_categories_report():
    def insert_text(cell, text):
        paragraph = cell.add_paragraph(str(text))
        paragraph.alignment = WD_ALIGN_PARAGRAPH.CENTER
        cell.vertical_alignment = WD_ALIGN_VERTICAL.CENTER

    report_doc = Document()

    report_doc.add_paragraph(datetime.now().strftime("%m.%d.%Y %H:%M:%S"))
    report_doc.add_paragraph('Sold Products by Categories')

    categories = Category.query.all()

    table = report_doc.add_table(len(categories) + 1, 3)

    table.style = 'Table Grid'

    insert_text(table.cell(0, 0), 'Name')
    insert_text(table.cell(0, 1), 'Number of products sold')
    insert_text(table.cell(0, 2), 'Total cost')

    for row_number, category in enumerate(categories):
        total_cost = 0
        number_of_products_sold = 0
        for product in category.products:
            if product.purchase_id:
                total_cost += product.price
                number_of_products_sold += 1

        insert_text(table.cell(row_number + 1, 0), category.name)
        insert_text(table.cell(row_number + 1, 1), number_of_products_sold)
        insert_text(table.cell(row_number + 1, 2), total_cost)

    report_doc.save(f'./app/static/reports/Products_by_Categories_{datetime.now().strftime("%m_%d_%Y_%H_%M_%S")}.docx')

    return {
        'status': 'success',
        'report_url': f'{APP_URL}/static/reports/Products_by_Categories_{datetime.now().strftime("%m_%d_%Y_%H_%M_%S")}.docx'
    }


@app.route('/admin/get_products_report', methods=['POST'])
@login_required(admin=True)
def get_products_report():
    def insert_text(cell, text):
        paragraph = cell.add_paragraph(str(text))
        paragraph.alignment = WD_ALIGN_PARAGRAPH.CENTER
        cell.vertical_alignment = WD_ALIGN_VERTICAL.CENTER

    report_doc = Document()

    report_doc.add_paragraph(datetime.now().strftime("%m.%d.%Y %H:%M:%S"))
    report_doc.add_paragraph('Stored Products')

    products = Product.query.filter_by(purchase_id=None).all()

    table = report_doc.add_table(len(products) + 1, 5)

    table.style = 'Table Grid'

    insert_text(table.cell(0, 0), 'Name')
    insert_text(table.cell(0, 1), 'Price')
    insert_text(table.cell(0, 2), 'Delivery date')
    insert_text(table.cell(0, 3), 'Category')
    insert_text(table.cell(0, 4), 'Manufacturer')

    for row_number, product in enumerate(products):
        insert_text(table.cell(row_number + 1, 0), product.name)
        insert_text(table.cell(row_number + 1, 1), product.price)
        insert_text(table.cell(row_number + 1, 2), product.delivery_date.strftime('%m.%d.%Y'))
        insert_text(table.cell(row_number + 1, 3), product.category.name)
        insert_text(table.cell(row_number + 1, 4), product.manufacturer.name)

    report_doc.save(f'./app/static/reports/Products_{datetime.now().strftime("%m_%d_%Y_%H_%M_%S")}.docx')

    return {
        'status': 'success',
        'report_url': f'{APP_URL}/static/reports/Products_{datetime.now().strftime("%m_%d_%Y_%H_%M_%S")}.docx'
    }


@app.route('/admin/get_statistics', methods=['POST'])
@login_required(admin=True)
def get_statistics():
    average_price_by_categories = [['Category', 'Average price']]
    average_storage_time_by_categories = [['Category', 'Average time']]
    for category in Category.query.all():
        sum_price = 0
        sum_time = 0
        for product in category.products:
            sum_price += product.price
            sum_time += (datetime.now().date() - product.delivery_date).days

        if len(category.products) != 0:
            average_price_by_categories.append([category.name, int(sum_price / len(category.products))])
            average_storage_time_by_categories.append([category.name, int(sum_time / len(category.products))])
        else:
            average_price_by_categories.append([category.name, 0])
            average_storage_time_by_categories.append([category.name, 0])

    average_price_by_manufacturers = [['Manufacturer', 'Average price']]
    for manufacturer in Manufacturer.query.all():
        sum_price = 0
        for product in manufacturer.products:
            sum_price += product.price

        if len(manufacturer.products) != 0:
            average_price_by_manufacturers.append([manufacturer.name, int(sum_price / len(manufacturer.products))])
        else:
            average_price_by_manufacturers.append([manufacturer.name, 0])

    total_purchase_cost_by_users = [['User', 'Total cost']]
    for user in User.query.all():
        sum_cost = 0
        for purchase in user.purchases:
            for product in purchase.products:
                sum_cost += product.price

        if len(user.purchases) != 0:
            total_purchase_cost_by_users.append([f'{user.name} {user.surname}', int(sum_cost / len(user.purchases))])
        else:
            total_purchase_cost_by_users.append([f'{user.name} {user.surname}', 0])

    return {
        'average_price_by_categories': average_price_by_categories,
        'average_price_by_manufacturers': average_price_by_manufacturers,
        'average_storage_time_by_categories': average_storage_time_by_categories,
        'total_purchase_cost_by_users': total_purchase_cost_by_users
    }


@app.route('/send_notifications', methods=['POST'])
@required_fields(['apikey'])
def send_notifications():
    data = request.get_json(force=True)

    if data['apikey'] != APIKEY:
        return {
            'status': 'fail',
            'message': 'Incorrect API key'
        }

    mail_body = render_template_string(
        '''
            {% for product in products %}
                <div>
                    <h1>{{ product.name }}</h1>
                    <h3>${{ product.price }}</h3>
                    <h5>Category: {{ product.category.name }}</h5>
                    <h5>Manufacturer: {{ product.manufacturer.name }}</h5>
                </div>
                
                <hr />
            {% endfor %}
        ''',
        products=Product.query.filter(
            Product.basket_id==None,
            Product.purchase_id==None,
            Product.delivery_date >= datetime.now().date() - timedelta(days=7)
        )
    )

    mail.send(
        Message(
            sender=MAIL_USERNAME,
            recipients=[user.email for user in User.query.all()],
            subject='New products in your favorite Secondhand',
            html=mail_body
        )
    )

    return {
        'status': 'success'
    }
