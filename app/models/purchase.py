from datetime import datetime
import uuid

from sqlalchemy.dialects.postgresql import UUID

from app import db


class Purchase(db.Model):
    __tablename__ = 'purchases'

    id = db.Column(UUID(), primary_key=True)

    user_id = db.Column(UUID(), db.ForeignKey('users.id'), nullable=False)

    datetime = db.Column(db.DateTime(), nullable=False)

    products = db.relationship('Product', backref='purchase')

    def __init__(self, user, products):
        self.id = uuid.uuid4().urn
        self.user = user
        self.datetime = datetime.now()
        self.products = products

    def __repr__(self):
        return f'Purchase of {self.user.name} {self.user.surname} with {len(self.products)} products'

    @property
    def json(self):
        return {
            'id': self.id,
            'products': [product.json for product in self.products]
        }
