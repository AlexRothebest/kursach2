import uuid

from sqlalchemy.dialects.postgresql import UUID

from app import db


class Category(db.Model):
    __tablename__ = 'categories'

    id = db.Column(UUID(), primary_key=True)

    name = db.Column(db.String(50), nullable=False, unique=True)

    products = db.relationship('Product', backref='category')

    def __init__(self, name):
        self.id = uuid.uuid4().urn
        self.name = name

    def __repr__(self):
        return f'Category {self.name}'

    @property
    def json(self):
        return {
            'id': self.id,
            'name': self.name
        }
