from datetime import datetime
import uuid

from sqlalchemy.dialects.postgresql import UUID

from app import db
from config import APP_URL


class Product(db.Model):
    __tablename__ = 'products'

    id = db.Column(UUID(), primary_key=True)

    name = db.Column(db.String(50), nullable=False)
    photo_path = db.Column(db.String(100), nullable=False)
    price = db.Column(db.Numeric(7, 2), nullable=False)
    delivery_date = db.Column(db.Date(), nullable=False)

    category_id = db.Column(UUID(), db.ForeignKey('categories.id'), nullable=False)
    manufacturer_id = db.Column(UUID(), db.ForeignKey('manufacturers.id'), nullable=False)

    basket_id = db.Column(UUID(), db.ForeignKey('baskets.id'), nullable=True)
    purchase_id = db.Column(UUID(), db.ForeignKey('purchases.id'), nullable=True)

    def __init__(self, name, price, category_id, manufacturer_id):
        self.id = uuid.uuid4().urn
        self.name = name
        self.price = price
        self.delivery_date = datetime.now().date()
        self.category_id = category_id
        self.manufacturer_id = manufacturer_id

    def __repr__(self):
        return f'Product {self.name}'

    @property
    def json(self):
        return {
            'id': self.id,
            'name': self.name,
            'photo_path': f'{APP_URL}{self.photo_path}',
            'price': float(self.price),
            'category': self.category.json,
            'manufacturer': self.manufacturer.json
        }
