import uuid

from sqlalchemy.dialects.postgresql import UUID

from app import db


class Basket(db.Model):
    __tablename__ = 'baskets'

    id = db.Column(UUID(), primary_key=True)

    user_id = db.Column(UUID(), db.ForeignKey('users.id'), nullable=False)

    products = db.relationship('Product', backref='basket')

    def __init__(self, user_id):
        self.id = uuid.uuid4().urn
        self.user_id = user_id

    def __repr__(self):
        return f'Basket of {self.user.name} {self.user.surname}'

    @property
    def json(self):
        return {
            'products': [product.json for product in self.products]
        }
