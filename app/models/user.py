import uuid

from flask_login import UserMixin
from sqlalchemy.dialects.postgresql import UUID

from app import db


class User(db.Model, UserMixin):
    __tablename__ = 'users'

    id = db.Column(UUID(), primary_key=True)

    name = db.Column(db.String(50), nullable=False, unique=True)
    surname = db.Column(db.String(50), nullable=False, unique=True)
    email = db.Column(db.String(100), nullable=False)
    phone = db.Column(db.String(50))
    password_hash = db.Column(db.String(200), nullable=False)
    is_admin = db.Column(db.Boolean(), nullable=False)

    basket = db.relationship('Basket', uselist=False, backref='user')
    purchases = db.relationship('Purchase', backref='user')

    def __init__(self, name, surname, email, phone, password_hash, is_admin=False):
        self.id = uuid.uuid4().urn
        self.name = name
        self.surname = surname or ''
        self.email = email
        self.phone = phone
        self.password_hash = password_hash
        self.is_admin = is_admin

    def __repr__(self):
        return f'Customer {self.name} {self.surname}'

    @property
    def json(self):
        return {
            'id': self.id,
            'name': self.name,
            'surname': self.surname,
            'email': self.email,
            'phone': self.phone,
            'is_admin': self.is_admin,
            'basket': self.basket.json
        }
