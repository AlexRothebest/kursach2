HOST = 'localhost'
PORT = 8000

DEBUG = True

SECRET_KEY = 'very secret key'

DATABASE_URI = 'postgres://postgres:password@host:port/database'

CLIENT_URL = ''
APP_URL = ''

APIKEY = ''

MAIL_SERVER = 'smtp.gmail.com'
MAIL_PORT = 587
MAIL_USE_TLS = True
MAIL_USE_SSL = False

MAIL_USERNAME = ''
MAIL_PASSWORD = ''
MAIL_DEFAULT_SENDER = 'Secondhand'
